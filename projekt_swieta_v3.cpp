#include <iostream>
#include <vector>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <cstdlib>

using namespace std;
static long LAST_INDEX=0;

string to_string( int t )
{
    stringstream ss;
    ss << t;
    return ss.str();
}

class Holiday{
private:
    string name;
    int day;
    int month;
    long lengthDay;
    long lengthNight;
    int sunriseHour;
    int sunriseMinute;
    int sunsetHour;
    int sunsetMinute;
    long index;

public:

    void setDay(int day){
        this->day = day;
    }
    void setMonth(int month){
        this->month = month;
    }
    void setLengthDay(long lengthDay){
        this->lengthDay = lengthDay;
    }
    void setLengthNight(long lengthNight){
        this->lengthNight = lengthNight;
    }
    void setSunriseHour(int sunriseHour){
        this->sunriseHour = sunriseHour;
    }
    void setSunriseMinute(int sunriseMinute){
        this->sunriseMinute = sunriseMinute;
    }
    void setName(string name){
        this->name = name;
    }
    void setIndex(long index){
        this->index = index;
    }
    void setSunsetHour(int sunsetHour){
        this->sunsetHour = sunsetHour;
    }
    void setSunsetMinute(int sunsetMinute){
        this->sunsetMinute = sunsetMinute;
    }


    string getName(){
        return name;
    }
    int getDay(){
        return day;
    }
    int getMonth(){
        return month;
    }
    long getLengthDay(){
        return lengthDay;
    }
    long getLengthNight(){
        return lengthNight;
    }
    int getSunriseHour(){
        return sunriseHour;
    }
    int getSunriseMinute(){
        return sunriseMinute;
    }
    long getIndex(){
        return index;
    }
    int getSunsetHour(){
        return sunsetHour;
    }
    int getSunsetMinute(){
        return sunsetMinute;
    }
};

class Formatter{
public:
    static string getFormatedDate(int day, int month){
        string dayString, monthString;

        if(day < 10){
            dayString = to_string(day);
            dayString = "0"+dayString;
        }
        else
            dayString = to_string(day);

        if(month < 10){
            monthString = to_string(month);
            monthString = "0"+monthString;
        }
        else
            monthString = to_string(month);

        return dayString + "." + monthString;
    }

    static string getFormatedLength(int minutes){
        int h, m;
        h = minutes / 60;
        m = minutes % 60;
        return to_string(h) + "h " +to_string(m) + "min";
    }

    static string getFormatedRow(Holiday *holiday){
        return holiday->getName() + ";" + to_string(holiday->getDay()) + ";" + to_string(holiday->getMonth()) + ";" + to_string(holiday->getSunriseHour()) + ";" + to_string(holiday->getSunriseMinute()) + ";" + to_string(holiday->getSunsetHour()) + ";" + to_string(holiday->getSunsetMinute());
    }
};

class Holidays{
private:
    vector<Holiday*> arrayHolidays;

    void split(const string& s, char c,
               vector<string>& v) {
       int i = 0;
       int j = s.find(c);

       while (j >= 0) {
          v.push_back(s.substr(i, j-i));
          i = ++j;
          j = s.find(c, j);

          if (j < 0) {
             v.push_back(s.substr(i, s.length()));
          }
       }
    }

public:
    string getDayName(Holiday *holiday, int year){
        const char * weekday[] = { "Niedziela", "Poniedzialek",
                                     "Wtorek", "Sroda",
                                     "Czwartek", "Piatek", "Sobota"};
        time_t rawtime;
        struct tm * timeinfo;
        time ( &rawtime );
        timeinfo = localtime ( &rawtime );
        timeinfo->tm_year = year - 1900;
        timeinfo->tm_mon = holiday->getMonth() - 1;
        timeinfo->tm_mday = holiday->getDay();
        mktime ( timeinfo );

        return weekday[timeinfo->tm_wday];
    }

    void addHoliday(Holiday *holiday){
        int hourDifference = holiday->getSunsetHour() - holiday->getSunriseHour();
        int sunsetMinute = holiday->getSunsetMinute();
        int sunriseMinute = holiday->getSunriseMinute();
        int minuteDifference;
        if(sunsetMinute < sunriseMinute){
            minuteDifference = sunsetMinute + sunriseMinute;
            hourDifference--;
        }
        else
            minuteDifference = sunsetMinute - sunriseMinute;

        holiday->setLengthDay((hourDifference*60) + minuteDifference);
        holiday->setLengthNight((24*60)-holiday->getLengthDay());
        LAST_INDEX++;
        holiday->setIndex(LAST_INDEX);
        arrayHolidays.push_back(holiday);
    }

    Holiday *getHolidayByIndex(long index){
        Holiday *holiday;
        for(int i=0; i<arrayHolidays.size(); i++){
            holiday = arrayHolidays[i];
            if(holiday->getIndex() == index)
                return holiday;
        }
        return NULL;
    }

    int getIndexOfHoliday(long index){
        Holiday *holiday;
        for(int i=0; i<arrayHolidays.size(); i++){
            holiday = arrayHolidays[i];

            if(holiday->getIndex() == index)
                return i;
        }
        return NULL;
    }

    void modifyHoliday(Holiday *holiday){
        int hourDifference = holiday->getSunsetHour() - holiday->getSunriseHour();
        int sunsetMinute = holiday->getSunsetMinute();
        int sunriseMinute = holiday->getSunriseMinute();
        int minuteDifference;
        if(sunsetMinute < sunriseMinute){
            minuteDifference = sunsetMinute + sunriseMinute;
            hourDifference--;
        }
        else
            minuteDifference = sunsetMinute - sunriseMinute;

        holiday->setLengthDay((hourDifference*60) + minuteDifference);
        holiday->setLengthNight((24*60)-holiday->getLengthDay());
        int indexOfArray = getIndexOfHoliday(holiday->getIndex());
        if(indexOfArray != NULL){
            arrayHolidays.at(indexOfArray) = holiday;
            cout << "Rekord zostal zmodyfikowany." << endl;
            return;
        }
        cout << "Niestety nie udalo sie zmodyfikowac rekordu." << endl;
    }

    void removeHoliday(long index){
        Holiday *holiday;
        bool removed = false;
        for(int i=0; i<arrayHolidays.size(); i++){
            holiday = arrayHolidays[i];
            if(holiday->getIndex() == index){
                arrayHolidays.erase(arrayHolidays.begin()+i);
                removed = true;
                break;
            }
        }
        if(removed)
            cout << "Rekord usuniety poprawnie" << endl;
        else
            cout << "Nie udalo sie usunac rekordu. Prawdopodobnie podany indeks jest niepoprawny." << endl;
    }

    void showHolidays(int year){
        Formatter formatter;
        cout << "**************************** Lista swiat dla roku " << year << " ****************************" <<endl;
        if(arrayHolidays.size()>0){
            Holiday *holiday;
            cout << setw(40) << left << "          Nazwa swieta " << setw(25) << left << "  Data " << setw(20) << left << "Dlugosc dnia(min)" << setw(20) << left << "Dlugosc nocy(min)" << setw(20) << left << "      Dzien" << " Indeks" <<endl;
            cout << endl;
            for(int i=0; i<arrayHolidays.size(); i++){
                holiday = arrayHolidays[i];
                cout << setw(40) << left << holiday->getName() << formatter.getFormatedDate(holiday->getDay(),holiday->getMonth())<<"."<< setw(25) << left <<year<< setw(20) << left << formatter.getFormatedLength(holiday->getLengthDay())<<setw(20) << left << formatter.getFormatedLength(holiday->getLengthNight()) <<setw(20) << left << getDayName(holiday,year) <<setw(20) << left << holiday->getIndex() <<endl;
            }
            cout << endl;
        }
        else{
            cout << endl;
            cout << "Nie dodano zadnych swiat do programu" << endl;
            cout << endl;
        }
        cout << endl;
        cout << endl;
    }

    void readFromFile(string path){
        string line;
        vector<string>* splitElements = NULL;
        ifstream in;
        in.open(path.c_str());
        if(in.fail()){
            cout << endl;
            cout << "Wystapil problem podczas otwierania pliku. Upewnij sie, ze podana sciezka jest poprawna." << endl;
            cout << endl;
            return;
        }
        Holiday *holiday;
        int converted;
        int imported=0;

        while(!in.eof()){
            holiday = new Holiday();
            getline(in,line,'\n');
            splitElements = new vector<string>();
            split(line, ';', *splitElements);
            holiday->setName(splitElements->at(0));
            converted = atoi(splitElements->at(1).c_str());
            holiday->setDay(converted);
            converted = atoi(splitElements->at(2).c_str());
            holiday->setMonth(converted);
            converted = atoi(splitElements->at(3).c_str());
            holiday->setSunriseHour(converted);
            converted = atoi(splitElements->at(4).c_str());
            holiday->setSunriseMinute(converted);
            converted = atoi(splitElements->at(5).c_str());
            holiday->setSunsetHour(converted);
            converted = atoi(splitElements->at(6).c_str());
            holiday->setSunsetMinute(converted);

            addHoliday(holiday);
            imported++;
        }
        cout << endl;
        cout << "Zaimportowane rekordy: " << imported << endl;
        cout << endl;
    }

    void saveToFile(string path, string filename){
        Formatter formatter;
        string resultPath = path+filename+".txt";
        ofstream saveFile(resultPath.c_str());
        for(int i=0; i<arrayHolidays.size(); i++){
            Holiday *holiday = arrayHolidays[i];
            saveFile << formatter.getFormatedRow(holiday) << endl;
        }

        saveFile.close();
    }
};


int main()
{
    int command = 1;
    Holidays *holidays = new Holidays();

    while(command != 0){
        cout << "0 -> Zakoncz" << endl;
        cout << "2 -> Wczytaj swieta z pliku" << endl;
        cout << "3 -> Dodaj swieta w konsoli" << endl;
        cout << "4 -> Wyswietl wszystkie swieta" << endl;
        cout << "5 -> Usun rekord" << endl;
        cout << "6 -> Modyfikuj rekord" << endl;
        cout << "7 -> Zapisz wszystkie zmiany do pliku" << endl;
        cin >> command;
        cout << endl;
        cout << endl;

        if(command == 0)
            break;
        else if(command == 2){
            cout << "Podaj sciezke do pliku z danymi. Obslugiwany format '.txt'." << endl;
            string path;
            cin >> path;
            // przyklad sciezki: 'C:/Users/Kamil/Desktop/swieta.txt'
            holidays->readFromFile(path);
        }
        else if(command == 3){
            Holiday *holiday = new Holiday();
            cout << "Podaj nazwe swieta" << endl;
            string name;
            cin.sync();
            getline(cin,name);
            cout << "Data swieta (w formacie 'dzien miesiac')" << endl;
            int day,month;
            cin >> day >> month;
            cout << "Czas wschodu slonca (w formacie 'godzina minuta')" << endl;
            int hour1, minute1;
            cin >> hour1 >> minute1;
            cout << "Czas zachodu slonca (w formacie 'godzina minuta')" << endl;
            int hour2, minute2;
            cin >> hour2 >> minute2;
            holiday->setName(name);
            holiday->setDay(day);
            holiday->setMonth(month);
            holiday->setSunriseHour(hour1);
            holiday->setSunriseMinute(minute1);
            holiday->setSunsetHour(hour2);
            holiday->setSunsetMinute(minute2);

            holidays->addHoliday(holiday);
            cout << "Swieto zostalo dodane do bazy" << endl;
            cout << endl;
        }
        else if(command == 4){
            int year;
            cout << "Podaj rok dla ktorego maja zostac wyswietlone swieta: " << endl;
            cin >> year;
            holidays->showHolidays(year);
        }
        else if(command == 5){
            cout << "Podaj indeks rekordu, ktory chcesz usunac. Jesli nie znasz indeksu wpisz '-1' i wyswietl wszystkie rekordy, by go znalezc." << endl;
            long index;
            cin >> index;

            if(index == -1)
                break;
            else
                holidays->removeHoliday(index);
        }
        else if(command == 6){
            cout << "Podaj indeks rekordu, ktory chcesz usunac. Jesli nie znasz indeksu wpisz '-1' i wyswietl wszystkie rekordy, by go znalezc." << endl;
            long index;
            cin >> index;

            if(index == -1)
                break;
            else{
                Holiday *holiday = holidays->getHolidayByIndex(index);
                if(holiday != NULL){
                    cout << "Jesli ktoregos pola nie chcesz modyfikowac wpisz '-1'." << endl;
                    string input;
                    cout << "Obecna nazwa swieta: " << holiday->getName() << endl;
                    cout << "Nowa nazwa swieta" << endl;
                    cin.sync();
                    getline(cin,input);

                    if(input != "-1")
                        holiday->setName(input);
                    int inputNumber;
                    cout << "Obecna data swieta: " << holiday->getDay() << "-" << holiday->getMonth() << endl;
                    cout << "Nowa data (w formacie 'dzien miesiac')" << endl;
                    cin >> inputNumber;
                    if(inputNumber != -1){
                        holiday->setDay(inputNumber);
                        cin >> inputNumber;
                        holiday->setMonth(inputNumber);
                    }
                    cout << "Obecna godzina i minuta: " << holiday->getSunriseHour() << ":" << holiday->getSunriseMinute() << endl;
                    cout << "Nowa godzina i minuta wschodu slonca (w formacie 'godzina minuta')" << endl;
                    cin >> inputNumber;
                    if(inputNumber != -1){
                        holiday->setSunriseHour(inputNumber);
                        cin >> inputNumber;
                        holiday->setSunriseMinute(inputNumber);
                    }
                    cout << "Obecna godzina i minuta: " << holiday->getSunsetHour() << ":" << holiday->getSunsetMinute() << endl;
                    cout << "Godzina i minuta zachodu slonca (w formacie 'godzina minuta')" << endl;
                    cin >> inputNumber;
                    if(inputNumber != -1){
                        holiday->setSunsetHour(inputNumber);
                        cin >> inputNumber;
                        holiday->setSunsetMinute(inputNumber);
                    }
                    holidays->modifyHoliday(holiday);
                }
                else
                    cout << "Niestety wystapil problem z modyfikacja danych." << endl;
            }
        }
        else if(command == 7){
            cout << "Podaj sciezke, w ktorej ma zostac zapisany plik: " << endl;
            string path;
            cin >> path;
            cout << "Podaj nazwe pliku, ktory ma zostac stworzony: " << endl;
            string filename;
            cin >> filename;
            holidays->saveToFile(path,filename);
        }
        else
            cout << "Podano nieprawidlowa komende" << endl;
    }

    delete holidays;
    return 0;
}
